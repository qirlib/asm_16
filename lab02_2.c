#include <stdio.h>

int main()
{
    unsigned number;
    
    printf("Enter number: ");
    if(scanf("%u", &number) != 1 || number == 0)
    {
        printf("Invalid input\n");
        return -1;
    }
    
    unsigned digit = number;
    while(digit / 10 != 0)
        digit /= 10;
        
    printf("First digit: %u\n", digit);
        
    int count = 0;
    while(number != 0)
    {
        if(number % 10 == digit)
            count++;
            
        number /= 10;
    }
    
    printf("Count = %d\n", count);
    
    return 0;
}

