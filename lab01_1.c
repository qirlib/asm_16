#include <stdio.h>

int main()
{
    double a;

    printf("Enter a: ");
    
    if(scanf("%lf", &a) != 1)
    {
        printf("Invalid input\n");
        return -1;
    }
    
    if(2 * a + a * a == 0 || 2 * a - a * a == 0)
    {
        printf("Division by zero\n");
        return -2;
    }
    
    double z = (1 + a + a * a) / (2 * a + a * a) + 2 - (1 - a + a * a) / (2 * a - a * a);
    z *= 5 - 2 * a * a;
    
    printf("Z = %lf\n", z);
    
    return 0;
}

