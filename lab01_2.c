#include <stdio.h>

int main()
{
    const double S = 100;
    
    printf("S = %.2lf km\n", S);
    
    double v, v1;
    printf("Enter V and V1 (km/h): ");
    
    if(scanf("%lf %lf", &v, &v1) != 2 || v <= 0 || v1 < 0 || v <= v1)
    {
        printf("Invalid input\n");
        return -1;
    }
    
    printf("t1 = %.2lfh, t2 = %.2lfh\n", S / v, S / (v - v1));
    
    return 0;
}

