#include <stdio.h>

int main()
{
    int number;
    
    printf("Enter 3-digit number: ");
    if(scanf("%d", &number) != 1 || number < 100 || number > 999)
    {
        printf("Invalid input\n");
        return -1;
    }
    
    int digit1 = number / 100;
    int digit2 = (number / 10) % 10;
    int digit3 = number % 10;
    
    printf("D1: %d\nD2: %d\nD3: %d\n", digit1, digit2, digit3);
    
    if(digit1 > digit3)
        printf("First digit is bigger than the third digit (D1 > D3)\n");
    else if(digit1 == digit3)
        printf("First digit is equal to the third digit (D1 = D3)\n");
    else
        printf("First digit is less than the third digit (D1 < D3)\n");
        
    if(digit2 > digit1)
        printf("Second digit is bigger than the first digit (D2 > D1)\n");
    else if(digit2 == digit1)
        printf("Second digit is equal to the first digit (D2 = D1)\n");
    else
        printf("Second digit is less than the first digit (D2 < D1)\n");
        
    if(digit2 > digit3)
        printf("Second digit is bigger than the third digit (D2 > D3)\n");
    else if(digit2 == digit3)
        printf("Second digit is equal to the third digit (D2 = D3)\n");
    else
        printf("Second digit is less than the third digit (D2 < D3)\n");
        
    if(digit1 >= digit2 && digit1 >= digit3)
        printf("First digit is the biggest (D1 >= D2; D1 >= D3)\n");
    else if(digit2 >= digit1 && digit2 >= digit3)
        printf("Second digit is the biggest (D2 >= D1; D2 >= D3)\n");
    else
        printf("Third digit is the biggest (D3 >= D1; D3 >= D2)\n");
    
    return 0;
}

